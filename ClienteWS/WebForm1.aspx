﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="ClienteWS.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WCF Services</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset>
			    <legend>Datos</legend>
                <asp:Label ID="LabeNombre" runat="server" Text="Nombre:"></asp:Label>
			    <asp:TextBox ID="Nombre" runat="server" Enabled="True"  MaxLength="80" ToolTip="Ingresa Tu Nombre" required="required"></asp:TextBox>
			    <asp:Button ID="BtnProcesar" runat="server" Text="Procesar" OnClick="BtnProcesar_Click" style="background-color:#3F7CBF;color:White;" />
                <br /><br />
                <asp:Label ID="LabeRespuesta" runat="server" Text="Respuesta" Style="font-family: Verdana; font-size:small;font-weight: bold;color:#b6d290;width:18%;"> </asp:Label>
                <br /><br />
		</fieldset>	
        </div>
    </form>
</body>
</html>


﻿using System;

namespace ClienteWS
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LabeRespuesta.Text = string.Empty;
            
        }

        protected void BtnProcesar_Click(object sender, EventArgs e)
        {
            var wnombre = Nombre.Text;
            LabeRespuesta.Text = string.Empty;
            using (WSHolaMundo.Service1Client client = new WSHolaMundo.Service1Client())
            {
                var saludo = client.HolaMundo(wnombre);
                LabeRespuesta.Text = saludo;

            }

        }

        
    }
}